## Cameron Getty's Resume – 2018

### Overview
[http://cjgetty.com](http://cjgetty.com/) • Based in California

I am an experience designer and art director currently pursuing a BFA at California State University, Sacramento. Recently, [I've been teaching myself programming](http://github.com/cjgetty).

My work is driven by curiosity. I often find the most satisfying solution to a problem lies within experimentation.

Also, I'm an [INFP](https://www.16personalities.com/infp-personality), if you're into that.

### Skills & Tools
There's always more to learn; always new ways to solve a problem.  My professional skills and tools include but are not limited to those listed above.

| Skills             | Tools           
| -------------      |:-------------:|
| Experience Design  | Adobe CC Suite|  
| Art Direction      | Sketch | Sketch|
| Creative Direction | InVision       |   
| Web Development    | Framer         |

### Experience
**Designer** • [Insight Communications + Design](http://insightpr.net/)\
Concepted, designed, and executed production of new campaigns for regionally-based startups to national brands.\
March 2018 – June 2019

**Design Student Assistant** • [California Conservation Corps](http://ccc.ca.gov/)\
Developed social media campaigns to reach a broader audience, and worked closely with the communications team to define a voice across all outlets.\
May 2017 – February 2018

### Education
California State Univerity, Sacramento\
Bachelor's Degree, Photography\
2016 – 2019

### Contact
* [Email](mailto:hello@cjgetty.com/)
* [Twitter](http://twitter.com/gettycj/)
* [Instagram](http://instagram.com/)
* [Dribbble](http://dribbble.com/cjgetty/)
* [Medium](http://medium.com/@cjgetty/)
* [GitHub](http://github.com/cjgetty)

### Thank You
To whomever may be reading this:

Thank you for taking the time to review my resume and for your consideration. If there's any more questions you may have about me, feel free to [email me](mailto:hello@cjgetty.com) or check out [my portfolio](http://cjgetty.com/) for work samples and more information.

**Made with ❤️**
